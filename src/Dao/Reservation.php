<?php
/**
 * Created by PhpStorm.
 * User: markus
 * Date: 9.03.16
 * Time: 12:41
 */

namespace Dao;

use Monolog\Handler\ErrorLogHandler;
use Monolog\Logger;

class Reservation
{
    private $_logger;
    private $_em;
    /**
     * @var \Repository\Reservation
     */
    private $_repository;

    public function __construct()
    {
        $this->_logger = new Logger(get_class($this));
        $this->_logger->pushHandler(new ErrorLogHandler());
        $this->_em = EntityManager::getInstance()->getManager();
        $this->_repository = $this->_em->getRepository('Model\Reservation');
    }

    /**
     * @param $id
     * @return \Model\Reservation
     */
    public function getReservation($id)
    {
        $reservation = $this->_repository->findOneBy(array('_id' => $id));
        $this->_logger->info('Reservation loaded successfully, Reservation details=' . $reservation);
        return $reservation;
    }

    /**
     * @return \DateTime
     */
    public function getMaxDate() {
        $date = new \DateTime();
        $maxDate = $this->_repository->getMaxDate();
        $date = $date::createFromFormat('Y-m-d H:i:s', $maxDate);
        return $date;
    }
    /**
     * @return \DateTime
     */
    public function getMinDate() {
        $date = new \DateTime();
        $minDate = $this->_repository->getMinDate();
        $date = $date::createFromFormat('Y-m-d H:i:s', $minDate);
        return $date;
    }
    /**
     * @param array $sorting
     * @return array
     */
    public function getReservationsSortedBy($sorting = array()) {
        $this->getMaxDate();
        $reservations = $this->_repository->findBy(array(), $sorting);
        foreach ($reservations as $reservation) {
            $this->_logger->info('Reservation::' . $reservation);
        }
        return $reservations;
    }
    /**
     * @param $filter array
     * @param array $ordering
     */
    public function getReservationsByFilter($filter, $ordering = array()) {
        $reservations = $this->_repository->getReservationsByFilter($filter, $ordering);
        foreach ($reservations as $reservation) {
            $this->_logger->info('Reservation::' . $reservation);
        }
        return $reservations;
    }
    /**
     * @return \Model\Reservation[]
     */
    public function getReservations()
    {
        $reservations = $this->_repository->findAll();
        foreach ($reservations as $reservation) {
            $this->_logger->info('Reservation::' . $reservation);
        }
        return $reservations;
    }

    /**
     * @param $reservation \Model\Reservation
     */
    public function update($reservation)
    {
        if(empty($reservation->getRestaurant())) throw new \Exceptions\NullException("Restaurant param is empty!");
        try {
            $this->_em->merge($reservation);
            $this->_logger->info('Reservation record updated successfully, Reservation details=' . $reservation);
        } catch (\Exception $e) {
            $this->_logger->critical($e->getMessage());
        }
    }

    public function delete($id)
    {
        $reservation = $this->getReservation($id);
        $this->_em->remove($reservation);
        $this->_logger->info('Reservation deleted successfully, Reservation details=' . $reservation);
        $this->_em->flush();
    }

    /**
     * @param $reservation \Model\Reservation
     * @return \Model\Reservation
     */
    public function add($reservation)
    {
        if(empty($reservation->getRestaurant())) throw new \Exceptions\NullException("Restaurant param is empty!");
        try {
            //$restaurantDao = new \Dao\Restaurant();
            //$restaurantDao->getRestaurant($reservation->getRestaurant());
            //$this->_em->persist($restaurant);
            $this->_em->persist($reservation);
            $this->_em->flush($reservation);
            $this->_logger->info('Reservation record saved successfully');
        } catch (\Exception $e) {
            $this->_logger->critical($e->getMessage());
        }
        return $reservation;
    }

    function __destruct()
    {
        $this->_em->flush();
    }
}