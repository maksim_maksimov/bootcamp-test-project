<?php
/**
 * Created by PhpStorm.
 * User: Maksim
 * Date: 2.04.2016
 * Time: 7:13
 */

namespace Dao;

use Monolog\Handler\ErrorLogHandler;
use Monolog\Logger;

class Restaurant
{
    private $_logger;
    private $_em;
    private $_repository;

    public function __construct()
    {
        $this->_logger = new Logger(get_class($this));
        $this->_logger->pushHandler(new ErrorLogHandler());
        $this->_em = EntityManager::getInstance()->getManager();
        $this->_repository = $this->_em->getRepository('Model\Restaurant');
    }

    /**
     * @param $id
     * @return null|\Model\Restaurant
     */
    public function getRestaurant($id)
    {
        $restaurant = $this->_repository->findOneBy(array('_id' => $id));
        $this->_logger->info('Restaurant loaded successfully, Restaurant details=' . $restaurant);
        return $restaurant;
    }

    /**
     * @return \Model\Restaurant[]
     */
    public function getRestaurants()
    {
        $restaurants = $this->_repository->findAll();
        foreach ($restaurants as $restaurant) {
            $this->_logger->info('Restaurant::' . $restaurant);
        }
        return $restaurants;
    }

    /**
     * @param $restaurant \Model\Restaurant
     */
    public function update($restaurant)
    {
        try {
            $oldRestaurant = $this->getRestaurant($restaurant->getId());
            $reservationRestaurants = $oldRestaurant->getReservations();
            $restaurant->setReservations($reservationRestaurants);
            $this->_em->merge($restaurant);
            $this->_logger->info('Restaurant record updated successfully, Reservation details=' . $restaurant);
        } catch (\PDOException $e) {
            $this->_logger->critical($e);
        }
    }

    public function delete($id)
    {
        $restaurant = $this->getRestaurant($id);
        $this->_em->remove($restaurant);
        $this->_logger->info('Restaurant deleted successfully, Restaurant details=' . $restaurant);
        $this->_em->flush();
    }

    /**
     * @param $restaurant
     * @return \Model\Restaurant
     */
    public function add($restaurant)
    {
        try {
            $this->_em->persist($restaurant);
            $this->_em->flush($restaurant);
            $this->_logger->info('Restaurant record saved successfully');
        } catch (\PDOException $e) {
            $this->_logger->critical($e);
        }
        return $restaurant;
    }

    function __destruct()
    {
        $this->_em->flush();
    }
}
