<?php
/**
 * Created by PhpStorm.
 * User: markus
 * Date: 9.03.16
 * Time: 12:05
 */

namespace Model;

use Doctrine\ORM\Mapping as ORM;
use Repository;

/**
 * @ORM\Entity(repositoryClass="Repository\Reservation")
 * @ORM\Table(name="reservation")
 */
class Reservation
{
    /**
     * @ORM\Id
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @var int
     */
    private $_id;

    /**
     * @ORM\ManyToOne(targetEntity="Restaurant", inversedBy="reservations", fetch = "EAGER")
     * @ORM\JoinColumn(name="restaurant_id", referencedColumnName="id", nullable=false)
     *
     * @var string
     */
    private $restaurant;

    /**
     * @ORM\Column(name="time", nullable=false, type="time")
     *
     * @var \DateTime
     */
    private $_time;

    /**
     * @ORM\Column(name="date", nullable=false, type="date")
     *
     * @var \DateTime
     */
    private $_date;

    /**
     * @ORM\Column(name="create_timestamp", nullable=false, type="datetime")
     *
     * @var \DateTime
     */
    private $_create_time;

    /**
     * @ORM\Column(name="edit_timestamp", nullable=true, type="datetime")
     *
     * @var \DateTime
     */
    private $_edit_time;

    /**
     * @ORM\Column(name="status", type="string", columnDefinition="ENUM('CREATED','CANCELED','DELETED','MODIFIED')")
     *
     * @var String
     */
    private $status;

    /**
     * @ORM\Column(name="duration", nullable=false, type="decimal", precision=1)
     *
     * @var double
     */
    private $_duration;

    /**
     * @ORM\Column(name="people_count", nullable=false, type="integer")
     *
     * @var int
     */
    private $_people_count;

    /**
     * @ORM\Column(name="contact_name", length=50, nullable=false)
     *
     * @var string
     */
    private $_contact_name;

    /**
     * @ORM\Column(name="type", length=10, nullable=false)
     *
     * @var string
     */
    private $_type;

    /**
     * @ORM\Column(name="contact_phone", length=10, nullable=true)
     *
     * @var string
     */
    private $_contact_phone;

    /**
     * @ORM\Column(name="comments", length=500, nullable=true)
     *
     * @var string
     */
    private $_comments;

    /**
     * @return \Model\Restaurant
     */
    public function getRestaurant()
    {
        return $this->restaurant;
    }

    /**
     * @param \Model\Restaurant $restaurant
     */
    public function setRestaurant($restaurant)
    {
        $this->restaurant = $restaurant;
    }

    /**
     * @return \DateTime
     */
    public function getTime()
    {
        return $this->_time;
    }
    public function getFormattedTime()
    {
        return !empty($this->_time) ? $this->_time->format('H:i') : '';
    }
    /**
     * @param \DateTime $time
     */
    public function setTime($time)
    {
        $this->_time = $time;
    }

    /**
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->_date;
    }
    public function getFormattedDate()
    {
        return !empty($this->_date) ? $this->_date->format('d.m.Y') : '';
    }
    /**
     * @param \DateTime $date
     */
    public function setDate($date)
    {
        $this->_date = $date;
    }

    /**
     * @return float
     */
    public function getDuration()
    {
        return $this->_duration;
    }

    /**
     * @param float $duration
     */
    public function setDuration($duration)
    {
        $this->_duration = $duration;
    }

    /**
     * @return int
     */
    public function getPeopleCount()
    {
        return $this->_people_count;
    }

    /**
     * @param int $people_count
     */
    public function setPeopleCount($people_count)
    {
        $this->_people_count = $people_count;
    }

    /**
     * @return string
     */
    public function getContactName()
    {
        return $this->_contact_name;
    }

    /**
     * @param string $contact_name
     */
    public function setContactName($contact_name)
    {
        $this->_contact_name = $contact_name;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->_type;
    }

    /**
     * @param string $type
     */
    public function setType($type)
    {
        $this->_type = $type;
    }

    /**
     * @return string
     */
    public function getContactPhone()
    {
        return $this->_contact_phone;
    }

    /**
     * @param string $contact_phone
     */
    public function setContactPhone($contact_phone)
    {
        $this->_contact_phone = $contact_phone;
    }

    /**
     * @return string
     */
    public function getComments()
    {
        return $this->_comments;
    }

    /**
     * @param string $comments
     */
    public function setComments($comments)
    {
        $this->_comments = $comments;
    }



    /**
     * @return int
     */
    public function getId()
    {
        return $this->_id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->_id = $id;
    }

    /**
     * @return \DateTime
     */
    public function getCreateTime()
    {
        return $this->_create_time;
    }

    /**
     * @param \DateTime $create_time
     */
    public function setCreateTime($create_time)
    {
        $this->_create_time = $create_time;
    }

    /**
     * @return \DateTime
     */
    public function getEditTime()
    {
        return $this->_edit_time;
    }

    /**
     * @param \DateTime $edit_time
     */
    public function setEditTime($edit_time)
    {
        $this->_edit_time = $edit_time;
    }

    /**
     * @param $status
     * @return bool
     */
    public function hasStatus($status) {
        return $status == $this->getStatus();
    }

    /**
     * @return String
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param String $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return string
     */
    function __toString()
    {
        return 'ID: ' . $this->_id . ', Restaurant: [' . $this->restaurant . '], Date: ' . $this->getFormattedDate() . ' ' . $this->getFormattedTime();
    }

}