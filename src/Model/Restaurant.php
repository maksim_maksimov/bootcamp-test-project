<?php
/**
 * Created by PhpStorm.
 * User: Maksim
 * Date: 2.04.2016
 * Time: 7:18
 */

namespace Model;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity
 * @ORM\Table(name="restaurant")
 */
class Restaurant
{
    /**
     * @ORM\Id
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @var int
     */
    private $_id;

    /**
     * @ORM\Column(name="name", length=30)
     *
     * @var string
     */
    private $_name;

    /**
     * @ORM\Column(name="address", length=100)
     *
     * @var string
     */
    private $_address;

    /**
     * @ORM\OneToMany(targetEntity="Reservation", mappedBy="restaurant")
     *
     * @var Collection
     */
    private $reservations;

    public function __construct()
    {
        $this->reservations = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->_id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->_id = $id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->_name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->_name = $name;
    }

    /**
     * @return string
     */
    public function getAddress()
    {
        return $this->_address;
    }

    /**
     * @param string $address
     */
    public function setAddress($address)
    {
        $this->_address = $address;
    }

    /**
     * @return Collection
     */
    public function getReservations()
    {
        return $this->reservations;
    }

    /**
     * @param Collection $reservations
     */
    public function setReservations($reservations)
    {
        $this->reservations = $reservations;
    }


    /**
     * @return string
     */
    function __toString()
    {
        return 'ID: ' . $this->_id . ', Name: ' . $this->_name . ', Address: ' . $this->_address;
    }
}

