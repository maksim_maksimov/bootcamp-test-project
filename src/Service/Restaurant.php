<?php
/**
 * Created by PhpStorm.
 * User: Maksim
 * Date: 2.04.2016
 * Time: 7:25
 */

namespace Service;


class Restaurant
{
    /**
     * @var \Dao\Restaurant
     */
    private $_dao;

    public function __construct()
    {
        $this->setDao(new \Dao\Restaurant());
    }

    public function setDao($dao)
    {
        $this->_dao = $dao;
    }

    /**
     * @return \Model\Restaurant[]
     */
    public function findAll()
    {
        $restaurants = $this->_dao->getRestaurants();
        return $restaurants;
    }

    public function findOneById($id)
    {
        $restaurant = $this->_dao->getRestaurant((int)$id);
        return $restaurant;
    }

    /**
     * @param $reservation \Model\Restaurant
     */
    public function update($restaurant)
    {
        $this->_dao->update($restaurant);
    }

    public function delete($id)
    {
        $this->_dao->delete((int)$id);
    }

    /**
     * @param $restaurant \Model\Restaurant
     * @return \Model\Restaurant
     */
    public function add($restaurant)
    {
        return $this->_dao->add($restaurant);
    }
}