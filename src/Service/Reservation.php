<?php
/**
 * Created by PhpStorm.
 * User: markus
 * Date: 9.03.16
 * Time: 12:41
 */

namespace Service;

class Reservation
{
    /**
     * @var \Dao\Reservation
     */
    private $_dao;

    public function __construct()
    {
        $this->setDao(new \Dao\Reservation());
    }

    public function setDao($dao)
    {
        $this->_dao = $dao;
    }

    /**
     * @param array $array Sorting
     * @return \Model\Reservation[]
     */
    public function getReservationsSortedBy($array = array()) {
        return $this->_dao->getReservationsSortedBy($array);
    }
    /**
     * @return array
     */
    public function findAll()
    {
        $reservations = $this->_dao->getReservations();
        return $reservations;
    }

    /**
     * @param $filter \Form\ReservationsFilter
     * @param array $ordering
     */
    public function getReservationsByFilter($filter, $ordering = array()) {
        return $this->_dao->getReservationsByFilter($filter->toObject(), $ordering);
    }
    /**
     * @param $id
     * @return \Model\Reservation
     */
    public function findOneById($id)
    {
        $reservation = $this->_dao->getReservation((int)$id);
        return $reservation;
    }

    /**
     * @param $reservation \Model\Reservation
     */
    public function update($reservation)
    {
        $reservation->setStatus("MODIFIED");
        $this->_dao->update($reservation);
    }

    public function delete($id)
    {
        $this->_dao->delete((int)$id);
    }

    /**
     * @param $reservation \Model\Reservation
     * @return \Model\Reservation
     */
    public function add($reservation)
    {
        return $this->_dao->add($reservation);
    }
    public function cancel($id) {
        $reservation = $this->_dao->getReservation((int)$id);
        $reservation->setStatus('CANCELED');
        $this->_dao->update($reservation);
    }
    public function getMaxDateTime() {
        return $this->_dao->getMaxDate();
    }
    public function getMinDateTime() {
        return $this->_dao->getMinDate();
    }
}