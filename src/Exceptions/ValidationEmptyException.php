<?php
/**
 * Created by PhpStorm.
 * User: Maksim
 * Date: 3.04.2016
 * Time: 3:22
 */

namespace Exceptions;

use Exception;


class ValidationEmptyException extends Exception
{
    public function __construct($message)
    {
        parent::__construct($message);
    }

    public function __toString()
    {
        return $this->message;
    }
}