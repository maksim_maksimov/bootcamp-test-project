<?php
/**
 * Created by PhpStorm.
 * User: Maksim
 * Date: 3.04.2016
 * Time: 23:03
 */

namespace Exceptions;

use Exception;

class ValidationErrorException extends Exception
{
    public function __construct($message)
    {
        parent::__construct($message);
    }

    public function __toString()
    {
        return $this->message;
    }

}