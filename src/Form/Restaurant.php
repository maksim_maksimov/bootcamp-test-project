<?php
/**
 * Created by PhpStorm.
 * User: Maksim
 * Date: 3.04.2016
 * Time: 21:30
 */

namespace Form;


class Restaurant extends AbstractForm
{
    /**
     * RestaurantForm constructor.
     * @param array $post
     */
    public function __construct($data = array())
    {
        $this->_required = array('name', 'address');
        parent::__construct($data);
    }

    /**
     * @param $data \Model\Restaurant
     */
    public function setObject($data) {
        if($this->objectCheck($data)) {
            parent::setObject($data);
            print_r($data->getName());
            $this->_properties = array(
                'id'                =>  $data->getId(),
                'name'              =>  $data->getName(),
                'address'           =>  $data->getAddress(),
            );
        }
    }

    /**
     * @return \Model\Restaurant
     */
    public function toObject() {
        parent::toObject();
        $required = $this->_required;
        $restaurant = new \Model\Restaurant();
        foreach($required as $param) {
            if(!in_array($param, array_keys($this->_properties)) || empty($this->_properties[$param])) {
                throw new \Exceptions\NullException("Missing required parameter '{$param}'!");
            }
        }
        foreach($required as $param) {
            $methodName = "set" . implode('', array_filter(explode('_', $param), 'ucfirst'));
            $restaurant->$methodName($this->_properties[$param]);
        }

        if(!empty($this->getProperty('id'))) $restaurant->setId($this->getProperty('id'));
        return $restaurant;
    }

    function objectCheck($data) {
        return $data instanceof \Model\Restaurant;
    }
}