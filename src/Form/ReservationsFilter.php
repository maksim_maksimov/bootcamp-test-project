<?php
/**
 * Created by PhpStorm.
 * User: Maksim
 * Date: 8.04.2016
 * Time: 1:13
 */

namespace Form;

use Exception;
use DateTime;
use Exceptions\NullException;

class ReservationsFilter extends AbstractForm
{
    private $_filters = array();
    /**
     * ReservationsFilter constructor.
     * @param array
     */
    public function __construct($data = array(), $filters = array())
    {
        parent::__construct($data);
        $this->_filters[] =  array(
            'value'     =>  'all',
            'option'    =>  'All reservations',
            'selected'  =>  true
        );
        foreach($filters as $filter) {
            if($filter instanceof \Model\Restaurant) {
                $this->_filters[] =  array(
                    'value'     =>  $filter->getId(),
                    'option'    =>  $filter->getName(),
                    'selected'  =>  false
                );
            }
        }
        //$this->_filters = array_merge($this->_filters, $filters);
        //print_r($this->_filters);
    }
    /**
     * @return array
     */
    public function toObject() {
        $arr = array();
        $arr = $this->_properties;
        return $arr;
    }
    /**
     * @param $data array
     */
    public function setObject($data) {
        if($this->objectCheck($data)) {
            parent::setObject($data);
            $this->_properties = array(
                'date_from'      =>  $data['date_from'],
                'date_to'        =>  $data['date_to'],
                'filter'         =>  $data['filter'],
            );
        }
    }
    /**
     * @param $data
     * @return bool
     */
    function objectCheck($data) {
        return true;
    }

    /**
     * Getting name of form.<br/>
     * <b>Like:</b> List of "all" reservations (23)
     *
     * @return String
     */
    function getFilterName($reservationsNum = 0) {
        return "List of " . ('"all" ') . "reservations ({$reservationsNum})";
    }

    /**
     * Gets selected filters.
     * @return String[][]
     */
    function getFilters() {
        $filters = array();
        foreach($this->_filters as &$filter) {
            if(in_array($filter['value'], $this->_properties['filter'])) $filter['selected'] = true;
            else $filter['selected'] = false;
        }
        return $this->_filters;
    }
}