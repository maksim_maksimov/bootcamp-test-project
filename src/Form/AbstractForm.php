<?php
/**
 * Created by PhpStorm.
 * User: Maksim
 * Date: 3.04.2016
 * Time: 9:03
 */

namespace Form;


class AbstractForm
{
    protected $id;
    protected $_properties;
    protected $_object;
    protected $_required = array();

    /**
     * AbstractForm constructor.
     * @param $post
     */
    public function __construct($data)
    {
        if(is_array($data)) {
            foreach($data as $key => $param) {
                $this->_properties[$key] = $param;
            }
        } else {
            if($this->objectCheck($data)) {
                $this->setObject($data);
            }
        }
    }

    /**
     * Function checking for appropriate object
     *
     * @return bool
     * @abstract
     */
    public function objectCheck($data) {
        return false;
    }
    public function setProperty($key, $prop) {
        $this->_properties[$key] = $prop;
    }
    public function getProperty($prop) {
        return isset($this->_properties[$prop]) && !empty($this->_properties[$prop]) ? $this->_properties[$prop] : '';
    }
    /**
     * Generate new object from form data
     *
     * @return mixed
     * @abstract
     */
    public function toObject() {
        return array();
    }
    /**
     * @param $data mixed
     * @abstract
     */
    public function setObject($data) {
        $this->object = $data;
    }
    /**
     * @return mixed
     */
    public function getProperties()
    {
        return $this->_properties;
    }

    /**
     * @param mixed $properties
     */
    public function setProperties($properties)
    {
        $this->_properties = $properties;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return array
     */
    public function getRequired()
    {
        return $this->_required;
    }

    /**
     * @param array $required
     */
    public function setRequired($required)
    {
        $this->_required = $required;
    }
}