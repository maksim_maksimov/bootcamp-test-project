<?php
/**
 * Created by PhpStorm.
 * User: Maksim
 * Date: 3.04.2016
 * Time: 0:57
 */

namespace Form;

use Exception;
use DateTime;
use Exceptions\NullException;

class Reservation extends AbstractForm
{
    /**
     * ReservationForm constructor.
     * @param array|\Model\Reservation $post
     */
    public function __construct($data = array())
    {
        $this->_required = array('date', 'time', 'duration', 'contact_name', 'people_count', 'type');
        parent::__construct($data);
    }

    /**
     * @param $data \Model\Reservation
     */
    public function setObject($data) {
        if($this->objectCheck($data)) {
            parent::setObject($data);
            $this->_properties = array(
                'id'                =>  $data->getId(),
                'restaurant'        =>  $data->getRestaurant()->getId(),
                'duration'          =>  $data->getDuration(),
                'date'              =>  $data->getFormattedDate(),
                'time'              =>  $data->getFormattedTime(),
                'type'              =>  $data->getType(),
                'people_count'      =>  $data->getPeopleCount(),
                'contact_name'      =>  $data->getContactName(),
                'contact_phone'     =>  $data->getContactPhone(),
                'comments'          =>  $data->getComments(),
            );
        }
    }

    /**
     * @return \Model\Reservation
     */
    public function toObject() {
        //print_r(get_object_vars(new \Model\Reservation()));
        $required = $this->_required;
        $reservation = new \Model\Reservation();
        foreach($required as $param) {
            if(!in_array($param, array_keys($this->_properties)) || empty($this->_properties[$param])) {
                throw new \Exceptions\NullException("Missing required parameter '{$param}'!");
            }
        }
        if(($key = array_search('date', $required)) !== false) unset($required[$key]);
        if(($key = array_search('time', $required)) !== false) unset($required[$key]);
        foreach($required as $param) {
            $methodName = "set" . implode('', array_filter(explode('_', $param), 'ucfirst'));
            $reservation->$methodName($this->_properties[$param]);
        }
        $reservation->setTime(DateTime::createFromFormat('H:i', $this->getProperty('time')));
        $reservation->setDate(DateTime::createFromFormat('d.m.Y', $this->getProperty('date')));

        if(!$reservation->getTime()) throw new NullException("Time field is empty!");
        if(!$reservation->getDate()) throw new NullException("Date field is empty!");

        $restaurantDao = new \Dao\Restaurant();
        $restaurant = $restaurantDao->getRestaurant($this->getProperty('restaurant'));
        if(!$restaurant) throw new NullException("Restaurant param is empty!");

        if(!empty($this->getProperty('contact_phone'))) $reservation->setContactPhone($this->getProperty('contact_phone'));
        if(!empty($this->getProperty('comments'))) $reservation->setComments($this->getProperty('comments'));
        if(!empty($this->getProperty('id'))) $reservation->setId($this->getProperty('id'));

        $reservation->setRestaurant($restaurant);

        return $reservation;
    }

    function objectCheck($data) {
        return $data instanceof \Model\Reservation;
    }

}