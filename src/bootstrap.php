<?php
// Load dependencies from composer
require_once(__DIR__ . "/../vendor/autoload.php");

// Load configuration
require_once(__DIR__ . '/Config/application.php');

// Load database connection params
require_once(__DIR__ . '/Config/database.php');