<?php
/**
 * Created by PhpStorm.
 * User: Maksim
 * Date: 3.04.2016
 * Time: 0:51
 */

namespace Validator;

use \Exceptions\NullException;

abstract class ValidatorAbstract
{
    protected $_errors;

    /**
     * @var \Form\Reservation
     */
    protected $_form;

    /**
     * ValidatorAbstract constructor.
     * @param null $form \Form\Reservation
     */
    public function __construct($form = NULL)
    {
        $this->_form = $form;
        $this->_errors = array();
    }

    /**
     * Main function for checking all fields in validator
     *
     * @throws \Exceptions\ValidationEmptyException
     */
    protected function checkFields() {
        $this->checkRequiredFields();
    }

    /**
     * Clears all errors from validator
     */
    public function clearFieldsErrors() {
        foreach($this->getErrors() as $field => $errors) {
            $this->_errors[$field] = array();
        }
    }

    /**
     * Checking all required form fields
     */
    public function checkRequiredFields() {
        if(empty($this->_form)) throw new NullException("Form property is null!");
        $properties = $this->_form->getProperties();
        foreach($this->_form->getRequired() as $param) {
            if(!in_array($param, array_keys($properties)) || $properties[$param] == "") {
                if(!isset($this->_errors[$param]) || !is_array($this->_errors[$param])) $this->_errors[$param] = array();
                $this->_errors[$param][] = new \Exceptions\ValidationEmptyException("Missing required parameter '{$param}'!");
            }
        }
    }

    /**
     * Get exception by field name.
     *
     * @param $field_name String
     * @return \Exception[]|null
     */
    public function getFieldErrors($field_name) {
        return isset($this->_errors[$field_name]) && !empty($this->_errors[$field_name]) ? $this->_errors[$field_name] : NULL;
    }
    /**
     * Get precision number
     *
     * @param $num String
     * @return int
     */
    function precision($num)
    {
        $places = substr($num, strpos($num, '.')+1);
        return strlen($places);
    }
    /**
     * @return \Form\AbstractForm
     */
    public function getForm()
    {
        return $this->_form;
    }

    /**
     * @param $form \Form\AbstractForm
     */
    public function setForm($form)
    {
        $this->_form = $form;
    }

    /**
     * Gets Exceptions where prioritized will bex <b>\Exceptions\NullException</b>.
     * Is used ion frontend, especially for frontend forms
     *
     * @return \Exception[]
     */
    public function getNullErrorsPriority() {
        $returnErrors = array();
        foreach($this->getErrors() as $field => $errors) {
            $currentErrors = array();
            foreach($errors as $key => $error) {
                if($error instanceof \Exceptions\ValidationEmptyException || $error instanceof \Exceptions\NullException) {
                    $currentErrors[] = $error;
                    break;
                }
            }
            if(count($errors) >= 1 && count($currentErrors) == 0) $currentErrors[] = $errors[0];
            $returnErrors = array_merge($returnErrors, $currentErrors);
        }
        return $returnErrors;
    }
    /**
     * Getter for errors.
     * Gets multidimensional array, where keys is field names.
     *
     * @return \Exception[][]
     */
    public function getErrors()
    {
        return $this->_errors;
    }

    /**
     * Getting all exceptions ass single array.
     *
     * @return \Exception[]
     */
    public function getErrorsList()
    {
        $returnErrors = array();
        foreach($this->getErrors() as $field => $errors) {
            foreach($errors as $key => $error) {
                $returnErrors[] = $error;
            }
        }
        return $returnErrors;
    }
    /**
     * @param array $errors
     */
    public function setErrors($errors)
    {
        $this->_errors = $errors;
    }

}