<?php
/**
 * Created by PhpStorm.
 * User: Maksim
 * Date: 8.04.2016
 * Time: 1:39
 */

namespace Validator;

use Exception;

class ReservationsFilter extends ValidatorAbstract
{
    public function __construct($form = NULL)
    {
        parent::__construct($form);
    }
    public function checkFields()
    {
        try {
            parent::checkfields();
        } catch(Exception $e) {
            $this->_errors[] = $e;
        }
        if(count($this->getErrors()) > 0) return false;
        return true;
    }
}