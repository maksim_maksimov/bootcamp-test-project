<?php
/**
 * Created by PhpStorm.
 * User: Maksim
 * Date: 3.04.2016
 * Time: 21:32
 */

namespace Validator;

use Exception;

class Restaurant extends ValidatorAbstract
{
    public function __construct($form = NULL)
    {
        parent::__construct($form);
    }
    public function checkFields()
    {
        try {
            parent::checkfields();
        } catch(Exception $e) {
            $this->_errors[] = $e;
        }
        try {
            $this->validateName($this->_form->getProperty('name'));
        } catch(Exception $e) {
            $this->_errors['name'][] = $e;
        }
        try {
            $this->validateAddress($this->_form->getProperty('address'));
        } catch(Exception $e) {
            $this->_errors['address'][] = $e;
        }
        if(count($this->getErrors()) > 0) return false;
        return true;
    }
    private function validateName($name) {
        if(strlen($name) < 3) throw new \Exceptions\ValidationErrorException("Name length must be more or equal 3!");
        if(strlen($name) > 30) throw new \Exceptions\ValidationErrorException("Name length is more than 30!");
    }
    private function validateAddress($address) {
        if(strlen($address) < 5) throw new \Exceptions\ValidationErrorException("Address length must be more or equal 5!");
        if(strlen($address) > 100) throw new \Exceptions\ValidationErrorException("Address length is more than 100!");
    }
}
