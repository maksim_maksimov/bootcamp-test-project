<?php
/**
 * Created by PhpStorm.
 * User: Maksim
 * Date: 3.04.2016
 * Time: 0:55
 */

namespace Validator;

use Exception;
use Exceptions\NullException;


class Reservation extends ValidatorAbstract
{
    public function __construct($form = NULL)
    {
        parent::__construct($form);
    }
    public function checkFields()
    {
        try {
            parent::checkfields();
        } catch(Exception $e) {
            $this->_errors[] = $e;
        }
        if(empty($this->_form)) throw new \Exceptions\NullException("form property is empty");
        try { // Date validation
            $this->validateDate($this->_form->getProperty('date'));
        } catch (Exception $e) {
            $this->_errors['date'][] = $e;
        }
        try { // Time validation
            $this->validateTime($this->_form->getProperty('time'));
        } catch (Exception $e) {
            $this->_errors['time'][] = $e;
        }
        try {// Duration validation
            $this->validateDuration($this->_form->getProperty('duration'));
        } catch (Exception $e) {
            $this->_errors['duration'][] = $e;
        }
        try {// People count validation
            $this->validatePeopleCount($this->_form->getProperty('people_count'));
        } catch (Exception $e) {
            $this->_errors['people_count'][] = $e;
        }
        try { // Contact name validation
            $this->validateContactName($this->_form->getProperty('contact_name'));
        } catch (Exception $e) {
            $this->_errors['contact_name'][] = $e;
        }
        try { // Type validation
            $this->validateType($this->_form->getProperty('type'));
        } catch (Exception $e) {
            $this->_errors['type'][] = $e;
        }
        try { // Contact phone validation
            $this->validateContactPhone($this->_form->getProperty('contact_phone'));
        } catch (Exception $e) {
            $this->_errors['contact_phone'][] = $e;
        }
        try { // Comments validation
            $this->validateComments($this->_form->getProperty('comments'));
        } catch (Exception $e) {
            $this->_errors['comments'][] = $e;
        }
        try{ // Date & time validation for been in past
            $this->validateForPastDate($this->_form->getProperty('date'), $this->_form->getProperty('time'));
        } catch(Exception $e) {
            $this->_errors['datetime'][] = $e;
        }
        if(count($this->getErrors()) > 0) return false;
        return true;
    }
    private function validateTime($time) {
        $dateTime = \DateTime::createFromFormat('H:i', $time);
        if(date_get_last_errors()['error_count'] > 0) throw new \Exceptions\ValidationErrorException("Invalid time format!");
    }
    private function validateDate($date) {
        $dateTime = \DateTime::createFromFormat('d.m.Y', $date);
        if(date_get_last_errors()['error_count'] > 0) throw new \Exceptions\ValidationErrorException("Invalid date format!");
    }
    private function validateComments($comments) {
        if(!empty($comments)) {
            if(strlen($comments) > 500) throw new \Exceptions\ValidationErrorException("Comments maximum length is 500!");
        }
    }
    private function validateContactPhone($phone) {
        if(!empty($phone)) {
            if(!is_numeric($phone)) throw new \Exceptions\ValidationErrorException("For contact phone only numbers allowed!");
            if(strlen($phone) < 5) throw new \Exceptions\ValidationErrorException("Contact name minimum length is 5!");
            if(strlen($phone) > 10) throw new \Exceptions\ValidationErrorException("Contact name maximum length is 10!");
        }
    }
    private function validateType($type) {
        if(!in_array($type, array('phone', 'email', 'other'))) throw new \Exceptions\ValidationErrorException("Invalid reservation type!");
    }
    private function validateContactName($name) {
        if(strlen($name) < 2) throw new \Exceptions\ValidationErrorException("Contact name minimum length is 2!");
        if(strlen($name) > 50) throw new \Exceptions\ValidationErrorException("Contact name maximum length is 50!");
    }
    private function validatePeopleCount($name) {
        if(!is_numeric($name)) throw new \Exceptions\ValidationErrorException("People number must be a number!");
        if((int)$name <= 0) throw new \Exceptions\ValidationErrorException("People number must be more than 1!");
        if((int)$name > 50) throw new \Exceptions\ValidationErrorException("People number is more than 50!");
    }
    private function validateDuration($name) {
        if(!is_numeric($name)) throw new \Exceptions\ValidationErrorException("Reservation duration is not number!");
        if($this->precision($name) > 1) throw new \Exceptions\ValidationErrorException("Reservation duration precision is 1!");
        if((int)$name <= 0) throw new \Exceptions\ValidationErrorException("Reservation duration must be more than 1!");
    }
    private function validateForPastDate($date, $time) {
        $dateTime = \DateTime::createFromFormat('d.m.Y H:i', "{$date} {$time}");
        if($dateTime < (new \DateTime("now"))) throw new \Exceptions\ValidationErrorException("Date & time can't be in past!");
    }
}