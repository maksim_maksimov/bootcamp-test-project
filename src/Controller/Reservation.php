<?php
/**
 * Created by PhpStorm.
 * User: markus
 * Date: 9.03.16
 * Time: 12:04
 */

namespace Controller;

use Exception;

class Reservation extends AbstractController
{
    /**
     * @var \Service\Reservation
     */
    private $_service;
    /**
     * @var \Service\Restaurant
     */
    private $_restaurantService;

    public function __construct()
    {
        parent::__construct();
        $this->setService(new \Service\Reservation());
        $this->_restaurantService = new \Service\Restaurant();
    }

    public function setService($service)
    {
        $this->_service = $service;
    }

    public function defaultView()
    {
        $post = $_POST;
        unset($post['path']);
        $reservationsFilterForm = new \Form\ReservationsFilter(array(
            'min_date_from'     =>  $this->_service->getMinDateTime()->format('d.m.Y'),
            'min_date_to'       =>  $this->_service->getMaxDateTime()->format('d.m.Y'),
            'filter'            =>  isset($post['filter']) && !empty($post['filter']) ? $post['filter'] : array(),
            'date_to'           =>  isset($post['date_to']) && !empty($post['date_to']) ? $post['date_to'] : $this->_service->getMaxDateTime()->format('d.m.Y'),
            'date_from'         =>  isset($post['date_from']) && !empty($post['date_from']) ? $post['date_from'] : $this->_service->getMinDateTime()->format('d.m.Y')
        ), $restaurants = $this->_restaurantService->findAll());
        $reservations = array();
        $errors = array();
        try {
            $reservationsFilterValidator = new \Validator\ReservationsFilter($reservationsFilterForm);
            $errors = $reservationsFilterValidator->getErrors();
            $reservations = $this->_service->getReservationsByFilter($reservationsFilterForm, array(
                '_date' => 'ASC',
                '_time' => 'ASC'
            ));
        } catch(Exception $e) {
            $errors[] = $e;
        } finally {
            $this->setModel(array(
                'reservations'  => $reservations,
                'errors'        => $errors,
                'filter'        => $reservationsFilterForm,
            ));
            return $this->display('reservations.twig');
        }
    }

    public function detailsView($id, $errors = array(), $successes = array())
    {
        $reservation = null;
        try {
            $reservation = $this->_service->findOneById((int)$id);
            if(isset($_GET['saved'])) {
                $successes[] = array(
                    "msg"       =>  "Reservation #{$id} successfully updated!",
                    "redirect"  =>  ""
                );
            }
        } catch(Exception $e) {
            $errors[] = $e;
        } finally {
            $this->setModel(array(
                'reservation' => $reservation,
                'successes'   => $successes,
                'errors' => $errors
            ));
            return $this->display('reservationDetails.twig');
        }
    }

    public function editView($id, $reservationForm = NULL, $errors = array(), $successes = array())
    {
        $restaurants = array();
        try {
            if($reservationForm == NULL) {
                $reservation = $this->_service->findOneById($id);
                $reservationForm = new \Form\Reservation($reservation);
            }
            $restaurants = $this->_restaurantService->findAll();
            //print_r($restaurants[0]->getReservations());
            if(isset($_GET['saved'])) {
                $successes[] = array(
                    "msg"       =>  "Reservation #{$id} successfully updated!",
                    "redirect"  =>  ""
                );
            }
        } catch(Exception $e) {
            $errors[] = $e;
        } finally {
            $this->setModel(array(
                'reservationData'       => $reservationForm,
                'errors'                => $errors,
                'successes'             => $successes,
                'restaurants'           => $restaurants
            ));
            return $this->display('editReservation.twig');
        }
    }

    public function deleteView($id)
    {
        $reservation = $this->_service->findOneById($id);
        $this->setModel(array('reservation' => $reservation));
        return $this->display('deleteReservation.twig');
    }

    public function cancelView($id)
    {
        $reservation = $this->_service->findOneById($id);
        $this->setModel(array('reservation' => $reservation));
        return $this->display('cancelReservation.twig');
    }

    public function createView($reservationForm = NULL, $errors = array())
    {
        if($reservationForm == NULL) {
            $reservationForm = new \Form\Reservation();
        }
        $restaurants = $this->_restaurantService->findAll();
        $this->setModel(array(
            'reservationData'       => $reservationForm,
            'errors'                => $errors,
            'restaurants'           => $restaurants
        ));
        return $this->display('newReservation.twig');
    }

    public function editAction()
    {
        $post = $_POST;
        $errors = array();
        $successes = array();
        $reservationForm = new \Form\Reservation($post);
        try {
            $reservationValidator = new \Validator\Reservation($reservationForm);
            if(!$reservationValidator->checkfields()) {
                $errors = $reservationValidator->getNullErrorsPriority();
            } else {
                $reservation = $reservationForm->toObject();
                $this->_service->update($reservation);
                $successes[] = array(
                    "msg"       =>  "Reservation #{$reservation->getId()} successfully updated!",
                    "redirect"  =>  "/reservation/edit/{$reservation->getId()}?saved"
                );
                //Select from db
                $reservation = $this->_service->findOneById($reservation->getId());
                $reservationForm = new \Form\Reservation($reservation);
            }
            return $this->editView($reservationForm->getProperty('id'), $reservationForm, $errors, $successes);
        } catch(Exception $e) {
            $errors[] = $e;
        } finally {
            return $this->editView($reservationForm->getProperty('id'), $reservationForm, $errors, $successes);
        }
    }

    public function cancelAction()
    {
        $post = $_POST;
        $this->_service->cancel(@$post['id']);
        return $this->defaultView();
    }

    public function deleteAction()
    {
        $post = $_POST;
        $this->_service->delete(@$post['id']);
        return $this->defaultView();
    }

    public function addAction()
    {
        $post = $_POST;
        $errors = array();
        $successes = array();
        $reservationForm = new \Form\Reservation($post);
        try {
            $reservationValidator = new \Validator\Reservation($reservationForm);
            if(!$reservationValidator->checkFields()) {
                $errors = $reservationValidator->getNullErrorsPriority();
            } else {
                $reservation = $reservationForm->toObject();
                $reservation = $this->_service->add($reservation);
                $reservationForm = new \Form\Reservation($reservation);
                $successes[] = array(
                    "msg"       =>  "Reservation #{$reservation->getId()} successfully added!",
                    "redirect"  =>  "/reservation/details/{$reservation->getId()}?saved"
                );
            }
        } catch(Exception $e) {
            $errors[] = $e;
        } finally {
            if(count($errors) > 0) return $this->createView($reservationForm, $errors);
            else return $this->detailsView($reservationForm->getProperty('id'), $errors, $successes);
        }
    }
}