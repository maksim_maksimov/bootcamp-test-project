<?php
/**
 * Created by PhpStorm.
 * User: Maksim
 * Date: 2.04.2016
 * Time: 7:24
 */

namespace Controller;

use \Exception;


class Restaurant extends AbstractController
{
    /**
     * @var \Service\Restaurant
     */
    private $_service;

    public function __construct()
    {
        parent::__construct();
        $this->setService(new \Service\Restaurant());
    }

    public function setService($service)
    {
        $this->_service = $service;
    }

    public function defaultView()
    {
        $restaurants = $this->_service->findAll();
        $this->setModel(array('restaurants' => $restaurants));
        return $this->display('restaurants.twig'); 
    }

    public function detailsView($id, $errors = array(), $successes = array())
    {
        $restaurant = null;
        try {
            $restaurant = $this->_service->findOneById((int)$id);
            if(isset($_GET['saved'])) {
                $successes[] = array(
                    "msg"       =>  "Restaurant '{$restaurant->getName()}' successfully added!",
                    "redirect"  =>  ""
                );
            }
        } catch(Exception $e) {
            $errors[] = $e;
        } finally {
            $this->setModel(array(
                'restaurant' => $restaurant,
                'successes'   => $successes,
                'errors' => $errors
            ));
            return $this->display('restaurantDetails.twig');
        }
    }

    /**
     * @param $id
     * @param \Form\Restaurant|null $restaurantForm
     * @param array $errors
     * @param array $successes
     * @return string
     */
    public function editView($id, $restaurantForm = NULL, $errors = array(), $successes = array())
    {
        try {
            if($restaurantForm == NULL) {
                $restaurant = $this->_service->findOneById($id);
                $restaurantForm = new \Form\Restaurant($restaurant);
            }
            if(isset($_GET['saved'])) {
                $successes[] = array(
                    "msg"       =>  "Restaurant #{$id} successfully updated!",
                    "redirect"  =>  ""
                );
            }
        } catch(Exception $e) {
            $errors[] = $e;
        } finally {
            $this->setModel(array(
                'restaurantData'        => $restaurantForm,
                'errors'                => $errors,
                'successes'             => $successes,
            ));
            return $this->display('editRestaurant.twig');
        }
    }

    public function deleteView($id)
    {
        $restaurant = $this->_service->findOneById($id);
        $this->setModel(array('restaurant' => $restaurant));
        return $this->display('deleteRestaurant.twig');
    }
    public function createView($restaurantForm = NULL, $errors = array())
    {
        if($restaurantForm == NULL) {
            $restaurantForm = new \Form\Restaurant();
        }
        $this->setModel(array(
            'restaurantData' => $restaurantForm,
            'errors'    =>  $errors
        ));
        return $this->display('newRestaurant.twig');
    }
    public function editAction()
    {
        $post = $_POST;
        $errors = array();
        $successes = array();
        $restaurantForm = new \Form\Restaurant($post);
        try {
            $restaurantValidator = new \Validator\Restaurant($restaurantForm);
            if(!$restaurantValidator->checkfields()) {
                $errors = $restaurantValidator->getNullErrorsPriority();
            } else {
                $restaurant = $restaurantForm->toObject();
                $this->_service->update($restaurant);
                //print_r($restaurant);
                $successes[] = array(
                    "msg"       =>  "Restaurant #{$restaurant->getId()} successfully updated!",
                    "redirect"  =>  "/restaurant/edit/{$restaurant->getId()}?saved"
                );
                //Select from db
                $restaurant = $this->_service->findOneById($restaurant->getId());
                $restaurantForm = new \Form\Restaurant($restaurant);
            }
        } catch(Exception $e) {
            $errors[] = $e;
        } finally {
            return $this->editView($restaurantForm->getProperty('id'), $restaurantForm, $errors, $successes);
        }
    }

    public function deleteAction()
    {
        $post = $_POST;
        $this->_service->delete(@$post['id']);
        return $this->defaultView();
    }

    public function addAction()
    {
        $post = $_POST;
        $errors = array();
        $successes = array();
        $restaurantForm = new \Form\Restaurant($post);
        try {
            $restaurantValidator = new \Validator\Restaurant($restaurantForm);
            if(!$restaurantValidator->checkFields()) {
                $errors = $restaurantValidator->getNullErrorsPriority();
            } else {
                $restaurant = $restaurantForm->toObject();
                $this->_service->add($restaurant);
                $restaurantForm = new \Form\Restaurant($restaurant);
                $successes[] = array(
                    "msg"       =>  "Restaurant #{$restaurant->getId()} successfully added!",
                    "redirect"  =>  "/restaurant/details/{$restaurant->getId()}?saved"
                );
            }
        } catch(\Exception $e) {
            $errors[] = $e;
        } finally {
            if(count($errors) > 0) return $this->createView($restaurantForm, $errors);
            else return $this->detailsView($restaurantForm->getProperty('id'), $errors, $successes);
        }
    }
}