<?php
/**
 * Created by PhpStorm.
 * User: Maksim
 * Date: 7.04.2016
 * Time: 22:15
 */

namespace Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\ResultSetMapping;
use PDO;

class Reservation extends EntityRepository
{
    /**
     * @param $filter
     * @param $sorting
     * @return array
     */
    public function getReservationsByFilter($filter, $sorting) {
        $qb = $this->_em->createQueryBuilder();
        $where = "";
        print_r($filter);
        if(!empty($filter['filter']) && !in_array('all', $filter['filter'])) {
            $where.="r.restaurant IN (:filters) AND ";
        }
        $from = \DateTime::createFromFormat('d.m.Y', $filter['date_from']);
        $to = \DateTime::createFromFormat('d.m.Y', $filter['date_to']);
        $query = $this->_em->createQuery("select r from Model\Reservation r WHERE {$where}r._date between :date_from and :date_to order by r._date asc, r._time asc");
        $params = array(
            'date_from'     =>  $from,
            'date_to'       =>  $to,
        );
        if(!empty($filter['filter'])) $params['filters'] = $filter['filter'];
        if(in_array('all', $filter['filter'])) unset($params['filters']);
        $query->setParameters($params);
        $reservations = $query->getResult();
        return $reservations;
    }
    /**
     * @return String|bool
     * @throws \Doctrine\DBAL\DBALException
     */
    public function getMaxDate(){
        $query = $this->_em->getConnection()->prepare('SELECT MAX(TIMESTAMP(date, time)) AS mex_date FROM reservation');
        $params = array();
        $query->execute($params);
        $result = $query->fetchAll(PDO::FETCH_COLUMN);
        return !empty($result) ? $result[0] : false;
    }
    /**
     * @return String|bool
     * @throws \Doctrine\DBAL\DBALException
     */
    public function getMinDate(){
        $query = $this->_em->getConnection()->prepare('SELECT MIN(TIMESTAMP(date, time)) AS mex_date FROM reservation');
        $params = array();
        $query->execute($params);
        $result = $query->fetchAll(PDO::FETCH_COLUMN);
        return !empty($result) ? $result[0] : false;
    }

}