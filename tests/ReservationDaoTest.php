<?php

/**
 * Created by PhpStorm.
 * User: Maksim
 * Date: 3.04.2016
 * Time: 4:29
 */
namespace Reservation;
require_once(__DIR__ . '/../src/bootstrap.php');


class ReservationDaoTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var \Dao\Reservation
     */
    protected $_dao;

    protected function setUp()
    {
        $this->_dao = new \Dao\Reservation();
    }

    /**
     * @expectedException \Exceptions\NullException
     */
    public function testForNullException() {
        $reservation = new \Model\Reservation();
        $this->_dao->add($reservation);
    }

    public function testForSuccessAdd() {
        $reservation = new \Model\Reservation();

        $restaurantDao = new \Dao\Restaurant();
        $restaurant = $restaurantDao->getRestaurants()[0];

        $reservation->setDate(new \DateTime("now"));
        $reservation->setRestaurant($restaurant);
        $reservation->setTime(new \DateTime("now"));
        $reservation->setDuration(12.5);
        $reservation->setContactName("Maxim Maximov");
        $reservation->setPeopleCount(10);
        $reservation->setType("phone");

        $newReservation = $this->_dao->add($reservation);
        $this->assertNotEmpty($newReservation);

        $this->_dao->delete($newReservation->getId());
    }
    public function testSuccessUpdate() {
        $restaurantDao = new \Dao\Restaurant();
        $oldReservation = $this->_dao->getReservations()[0];

        $reservation = $oldReservation;
        $reservation->setDate(new \DateTime("now"));
        $reservation->setTime(new \DateTime("now"));
        $reservation->setDuration(12.5);
        $reservation->setContactName("Maxim Maximov");
        $reservation->setPeopleCount(10);
        $reservation->setType("phone");

        $restaurant = $restaurantDao->getRestaurants()[0];

        $reservation->setRestaurant($restaurant);

        $this->_dao->update($reservation);
        $this->assertEquals($oldReservation->getId(), $reservation->getId());
        $this->assertEquals($oldReservation->getFormattedTime(), $reservation->getFormattedTime());

        //setting back
        $this->_dao->update($oldReservation);
    }
}