<?php
/**
 * Created by PhpStorm.
 * User: Maksim
 * Date: 5.04.2016
 * Time: 20:19
 */

namespace Restaurant;


class RestaurantValidatorTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var \Validator\Restaurant
     */
    protected $validator;
    /**
     * @var \Form\Restaurant
     */
    protected $restaurantForm;

    /**
     * @var \Form\Restaurant
     */
    protected $prevForm;

    protected function setUp()
    {
        $this->validator = new \Validator\Restaurant();
        $this->restaurantForm = new \Form\Restaurant(array(
            'id'        => 1,
            'name'      => '05.04.2016',
            'address'   => 'asdasd'
        ));
        $this->validator->setForm($this->restaurantForm);
    }

    protected function assertPreConditions()
    {
        $this->prevForm = $this->validator->getForm();
    }

    protected function assertPostConditions()
    {
        //$this->validator->setForm($this->prevForm); //setting back
    }
    public function testCheckingRequiredFieldsSuccess() {
        $this->validator->checkRequiredFields();
        $this->assertEquals(0, count($this->validator->getErrorsList()));
    }
    public function testCheckingRequiredFieldsFail() {
        $form = $this->prevForm;

        $form->setProperty('name', '');
        $form->setProperty('address', '');

        $this->validator->setForm($form);
        $this->validator->checkRequiredFields();
        $this->assertEquals(2, count($this->validator->getErrorsList())); //2 null
        $this->assertNotNull($this->validator->getFieldErrors('name'));
        $this->assertNotNull($this->validator->getFieldErrors('address'));
    }
    public function testCheckingFieldsSuccess() {
        $this->validator->checkFields();
        $this->assertEquals(0, count($this->validator->getErrorsList()));
    }
    public function testValidateAddressFail() {
        $form = $this->prevForm;

        $form->setProperty('address', 'Assd');

        $this->validator->setForm($form);
        $this->validator->checkFields();

        $this->assertEquals(1, count($this->validator->getErrorsList()));
        $this->assertNotNull($this->validator->getFieldErrors('address'));

        $form->setProperty('address', 'sdsfdsfsdfsdfsdfsdfsdfdsfsdfsdfdsfsdfdsfdsfsdfdsfsdfdsf');

        $this->validator->setForm($form);
        $this->validator->checkFields();

        $this->assertEquals(1, count($this->validator->getErrorsList()));
        $this->assertNotNull($this->validator->getFieldErrors('address'));
    }
    public function testValidateNameFail() {
        $form = $this->prevForm;

        $form->setProperty('name', 'as');

        $this->validator->setForm($form);
        $this->validator->checkFields();

        $this->assertEquals(1, count($this->validator->getErrorsList()));
        $this->assertNotNull($this->validator->getFieldErrors('name'));

        $this->validator->clearFieldsErrors();
        $form->setProperty('name', 'sdsfdsfsdfsdfsdfsdfsdfdsfsdfsdfdsfsdfdsfdsfsdfdsfsdfdsfsadsadasdsadasdd');

        $this->validator->setForm($form);
        $this->validator->checkFields();

        $this->assertEquals(1, count($this->validator->getErrorsList()));
        $this->assertNotNull($this->validator->getFieldErrors('name'));
    }
}