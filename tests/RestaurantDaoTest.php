<?php
/**
 * Created by PhpStorm.
 * User: Maksim
 * Date: 3.04.2016
 * Time: 22:25
 */

namespace Restaurant;

use Doctrine\DBAL\Driver;

require_once(__DIR__ . '/../src/bootstrap.php');

class RestaurantDaoTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var \Dao\Restaurant
     */
    protected $_dao;

    protected function setUp()
    {
        $this->_dao = new \Dao\Restaurant();
    }
    /**
     *
     * \Doctrine\DBAL\Exception\NotNullConstraintViolationException
     */
//    public function testForNullException() {
//        $restaurant = new \Model\Restaurant();
//        $this->_dao->add($restaurant);
//    }

//    /**
//     * \Doctrine\DBAL\Exception\UniqueConstraintViolationException
//     */
//    public function testForSuccessSameNameAddFail() {
//        $restaurant = new \Model\Restaurant();
//
//        $restaurant->setName("Restoraan test name");
//        $restaurant->setAddress("Restoraan test address");
//
//        $newRestaurant = $this->_dao->add($restaurant);
//        $this->assertNotNull($newRestaurant);
//
//        $this->_dao->delete($newRestaurant->getId());
//    }
    public function testForSuccessAdd() {
        $restaurant = new \Model\Restaurant();

        $restaurant->setName("Restoraan test namedfsf");
        $restaurant->setAddress("Restoraan test addressdsfsdf");

        $newRestaurant = $this->_dao->add($restaurant);
        $this->assertNotNull($newRestaurant);

        $this->_dao->delete($newRestaurant->getId());
    }

    public function testSuccessUpdate() {
        $oldRestaurant = $this->_dao->getRestaurants()[0];

        $oldAddress = $oldRestaurant->getAddress();
        $oldName = $oldRestaurant->getAddress();

        $restaurant = $oldRestaurant;

        $restaurant->setName("Restoraan test namedsfs");
        $restaurant->setAddress("Restoraan test addresssdfsd");
        $restaurant->setId($oldRestaurant->getId());

        $this->_dao->update($restaurant);
        $this->assertNotSame($oldAddress, $restaurant->getAddress());
        $this->assertNotSame($oldName, $restaurant->getName());

        //setting back
        $this->_dao->update($oldRestaurant);
    }
}