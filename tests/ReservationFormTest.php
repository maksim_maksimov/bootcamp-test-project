<?php

/**
 * Created by PhpStorm.
 * User: Maksim
 * Date: 3.04.2016
 * Time: 8:58
 */
namespace Reservation;
require_once(__DIR__ . '/../src/bootstrap.php');


class ReservationFormTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var \Form\Reservation
     */
    protected $_reservationForm;
    /**
     * @var \Model\Restaurant
     */
    protected $_restaurant;

    protected function setUp()
    {
        $restaurantDao = new \Dao\Restaurant();
        $this->_restaurant = $restaurantDao->getRestaurants()[0];
        $this->_reservationForm = new \Form\Reservation(array(
            'id'            =>  1,
            'date'          =>  '3.04.2014',
            'restaurant'    =>  $this->_restaurant->getId(),
            'time'          =>  '12:30',
            'duration'      =>  12,
            'contact_name'  =>  'sdfsdf',
            'people_count'  =>  123,
            'type'          =>  'other'
        ));
    }
    public function testReservationFormToReservationObject(){
        $reservation = $this->_reservationForm->toObject();
        $this->assertInstanceOf('\Model\Reservation', $reservation);
    }


}