<?php
/**
 * Created by PhpStorm.
 * User: Maksim
 * Date: 3.04.2016
 * Time: 3:53
 */
namespace Reservation;

class ReservationValidatorTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var \Validator\Reservation
     */
    protected $validator;
    /**
     * @var \Form\Reservation
     */
    protected $reservationForm;

    /**
     * @var \Form\Reservation
     */
    protected $prevForm;

    protected function setUp()
    {
        $this->validator = new \Validator\Reservation();
        $this->reservationForm = new \Form\Reservation(array(
            'id'            =>  1,
            'date'          =>  (new \DateTime('now +1 day'))->format("d.m.Y"),
            'restaurant'    =>  1,
            'time'          =>  (new \DateTime('now +1 day'))->format("H:i"),
            'duration'      =>  12,
            'contact_name'  =>  'sdfsdf',
            'people_count'  =>  50,
            'type'          =>  'other'
        ));
        $this->validator->setForm($this->reservationForm);
    }
    protected function assertPreConditions()
    {
        $this->prevForm = $this->validator->getForm();
    }
    protected function assertPostConditions()
    {
        //$this->validator->setForm($this->prevForm); //setting back
    }
    public function testCheckingRequiredFieldsSuccess() {
        $this->validator->checkRequiredFields();
        $this->assertEquals(0, count($this->validator->getErrorsList()));
    }
    public function testCheckingRequiredFieldsFail() {
        $form = $this->prevForm;

        $form->setProperty('duration', '');
        $form->setProperty('time', '');

        $this->validator->setForm($form);
        $this->validator->checkRequiredFields();

        $this->assertEquals(2, count($this->validator->getErrorsList())); //2 null
        $this->assertNotNull($this->validator->getFieldErrors('duration'));
        $this->assertNotNull($this->validator->getFieldErrors('time'));
    }
    public function testCheckingFieldsSuccess() {
        $this->validator->checkFields();
        $this->assertEquals(0, count($this->validator->getErrorsList()));
    }
    public function testValidateDateFail() {
        $form = $this->prevForm;

        $form->setProperty('date', '12.2ss.1222');

        $this->validator->setForm($form);
        $this->validator->checkFields();

        $this->assertEquals(2, count($this->validator->getErrorsList()));
        $this->assertNotNull($this->validator->getFieldErrors('date'));
    }
    public function testValidateTimeFail() {
        $form = $this->prevForm;

        $form->setProperty('time', '12:12:12');

        $this->validator->setForm($form);
        $this->validator->checkFields();

        $this->assertEquals(2, count($this->validator->getErrorsList()));
        $this->assertNotNull($this->validator->getFieldErrors('time'));
    }
    public function testValidateDurationFail() {
        $form = $this->prevForm;

        $form->setProperty('duration', '0');

        $this->validator->setForm($form);
        $this->validator->checkFields();

        $this->assertEquals(1, count($this->validator->getErrorsList()));
        $this->assertNotNull($this->validator->getFieldErrors('duration'));

        $form->setProperty('duration', '-1'); //negative

        $this->validator->setForm($form);
        $this->validator->checkFields();

        $form->setProperty('duration', '1.22'); //precision 2

        $this->validator->setForm($form);
        $this->validator->checkFields();

        $this->assertEquals(3, count($this->validator->getErrorsList()));
        $this->assertEquals(3, count($this->validator->getFieldErrors('duration')));
    }
    public function testValidatePeopleCountFail() {
        $form = $this->prevForm;

        $form->setProperty('people_count', 'asa');

        $this->validator->setForm($form);
        $this->validator->checkFields();

        $this->assertEquals(1, count($this->validator->getErrorsList()));
        $this->assertNotNull($this->validator->getFieldErrors('people_count'));

        $form->setProperty('people_count', '0'); //zero

        $this->validator->setForm($form);
        $this->validator->checkFields();

        $form->setProperty('people_count', '51'); //more than 50

        $this->validator->setForm($form);
        $this->validator->checkFields();

        $this->assertEquals(3, count($this->validator->getErrorsList()));
        $this->assertEquals(3, count($this->validator->getFieldErrors('people_count')));
    }
    public function testValidateContactNameFail() {
        $form = $this->prevForm;

        $form->setProperty('contact_name', 'a');

        $this->validator->setForm($form);
        $this->validator->checkFields();

        $this->assertEquals(1, count($this->validator->getErrorsList()));
        $this->assertNotNull($this->validator->getFieldErrors('contact_name'));

        $form->setProperty('contact_name', '0sfsdfsdsfsdfdsfdfsasdsadfasdfgfdsgfdscdsfgrfadsfasfdsfgsdf'); //more than 50

        $this->validator->setForm($form);
        $this->validator->checkFields();

        $this->assertEquals(2, count($this->validator->getErrorsList()));
        $this->assertEquals(2, count($this->validator->getFieldErrors('contact_name')));
    }
    public function testValidateContactPhoneFail() {
        $form = $this->prevForm;

        $form->setProperty('contact_phone', 'a');

        $this->validator->setForm($form);
        $this->validator->checkFields();

        $this->assertEquals(1, count($this->validator->getErrorsList()));
        $this->assertNotNull($this->validator->getFieldErrors('contact_phone'));

        $form->setProperty('contact_phone', '0sfsdfsdsfsdfdsfdfsasdsadfasdfgfdsgfdscdsfgrfadsfasfdsfgsdf'); //more than 10

        $this->validator->setForm($form);
        $this->validator->checkFields();

        $this->assertEquals(2, count($this->validator->getErrorsList()));
        $this->assertEquals(2, count($this->validator->getFieldErrors('contact_phone')));
    }

    public function testValidateType() {
        $form = $this->prevForm;

        $form->setProperty('type', 'internet');

        $this->validator->setForm($form);
        $this->validator->checkFields();

        $this->assertEquals(1, count($this->validator->getErrorsList()));
        $this->assertNotNull($this->validator->getFieldErrors('type'));

        $form->setProperty('type', 'phone');

        $this->validator->setForm($form);
        $this->validator->checkFields();

        $this->assertEquals(1, count($this->validator->getErrorsList()));
        $this->assertEquals(1, count($this->validator->getFieldErrors('type')));
    }
    public function testValidatePastDateFail() {
        $form = $this->prevForm;

        $form->setProperty('date', 'a');
        $form->setProperty('time', 'a');

        $this->validator->setForm($form);
        $this->validator->checkFields();

        $this->assertEquals(3, count($this->validator->getErrorsList()));
        $this->assertNotNull($this->validator->getFieldErrors('datetime'));
    }
}