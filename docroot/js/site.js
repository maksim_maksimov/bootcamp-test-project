/**
 * Created by markus on 9.03.16.
 */
$(document).ready(function() {
    // validate registration form
    $.validator.methods.number = function (value, element) {
        return this.optional(element) || /^-?(?:\d+|\d{1,3}(?:[\s\.,]\d{3})+)(?:[\.,]\d{1})?$/.test(value);
    };
    var rules = {
        reservation: {
            time: {
                required: true
            },
            duration: {
                required: true,
                number: true

            },
            restaurant: {
                required: true
            },
            date: {
                required: true
            },
            people_count: {
                required: true,
                range: [1, 50]
            },
            contact_name: {
                required: true,
                rangelength: [2, 50]
            },
            contact_phone: {
                rangelength: [5, 10],
                digits: true
            },
            comments: {
                rangelength: [0, 500]
            },
            type: {
                required: true
            }
        },
        restaurant: {
            name: {
                required: true,
                rangelength: [3, 30]
            },
            address: {
                required: true,
                rangelength: [5, 100]
            }
        }
    };
    function validateForm($object, rules) {
        $object.validate({
            rules: rules,
            highlight: function(element) {
                var $element = $(element),
                    type = $element.attr('type');
                if(type == 'radio') {
                    $(element).closest('.form-group').addClass('has-error');
                } else {
                    $(element).closest('.form-cell').addClass('has-error');
                }
            },
            unhighlight: function(element) {
                var $element = $(element),
                    type = $element.attr('type');
                if(type == 'radio') {
                    $(element).closest('.form-group').removeClass('has-error');
                } else {
                    $(element).closest('.form-cell').removeClass('has-error');
                }
            },
            errorElement: 'span',
            errorClass: 'help-block',
            errorPlacement: function(error, element) {
                var $element = $(element),
                    type = $element.attr('type');
                if(type == 'radio') {
                    element.parent().parent().parent().append(error);
                } else {
                    if (element.parent('.input-group').length) {
                        error.insertAfter(element.parent());
                    } else {
                        error.insertAfter(element);
                    }
                }
            }
        });
    }
    validateForm($(".reservation-form"), rules.reservation);
    validateForm($(".restaurant-form"), rules.restaurant);
    $('#timepicker').datetimepicker({
        format: 'HH:mm'
    });
    $('#datepicker').datetimepicker({
        format: 'DD.MM.YYYY'
    });
    datePicker($('#reservations-date-from'));
    datePicker($('#reservations-date-to'));
    function datePicker($object) {
        var min = $object.data('min').split('.'),
            minDate = new Date(parseInt(min[2]), parseInt(min[1]) - 1, parseInt(min[0])),
            max = $object.data('max').split('.'),
            maxDate = new Date(parseInt(max[2]), parseInt(max[1]) - 1, parseInt(max[0])),
            current = $object.val().split('.'),
            currentDate = new Date(parseInt(current[2]), parseInt(current[1]) - 1, parseInt(current[0]));
        $object.datetimepicker({
            format: 'DD.MM.YYYY',
            minDate: minDate,
            defaultDate: currentDate,
            maxDate: maxDate
        });
    }
    $('.navbar-nav a').each(function(){
        var href = $(this).attr('href');
        if(location.pathname.indexOf(href) != -1) {
            $(this).parent().addClass('active');
        }
    });
    if(window.location.search == "?saved") {
        setTimeout(function(){
            $('.alert.alert-success').fadeOut(350);
        }, 1000);
    }
    $('[data-toggle="tooltip"]').tooltip();
    $("select").chosen({disable_search_threshold: 10});
});