-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 06, 2016 at 11:16 AM
-- Server version: 10.1.10-MariaDB
-- PHP Version: 7.0.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `reservations`
--

-- --------------------------------------------------------

--
-- Table structure for table `reservation`
--

CREATE TABLE `reservation` (
  `id` int(11) NOT NULL,
  `restaurant_id` int(11) NOT NULL,
  `time` time DEFAULT NULL,
  `date` date NOT NULL,
  `duration` decimal(10,0) NOT NULL,
  `people_count` int(11) NOT NULL,
  `contact_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `contact_phone` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `comments` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `create_timestamp` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `edit_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` enum('CREATED','CANCELED','DELETED','MODIFIED','') COLLATE utf8_unicode_ci DEFAULT 'CREATED'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `reservation`
--

INSERT INTO `reservation` (`id`, `restaurant_id`, `time`, `date`, `duration`, `people_count`, `contact_name`, `type`, `contact_phone`, `comments`, `create_timestamp`, `edit_timestamp`, `status`) VALUES
(61, 1, '12:15:00', '2016-04-04', '3', 5, 'Andreis', 'phone', NULL, NULL, '2016-04-03 17:28:51', '2016-04-05 22:46:47', 'CREATED'),
(79, 2, '07:03:18', '2016-04-06', '13', 10, 'Maxim Maximov', 'phone', NULL, NULL, '2016-04-04 00:13:02', '2016-04-06 04:03:18', 'CREATED'),
(80, 3, '09:18:26', '2016-04-06', '13', 10, 'Maxim Maximov', 'phone', NULL, NULL, '2016-04-04 00:16:34', '2016-04-06 06:18:26', 'CREATED'),
(89, 2, '01:00:00', '2016-04-12', '1', 13, 'Asdas', 'email', NULL, NULL, '2016-04-04 18:04:53', '2016-04-05 22:46:47', 'CREATED'),
(90, 3, '01:00:00', '2016-04-13', '5', 5, 'Asdas', 'email', NULL, NULL, '2016-04-04 18:06:01', '2016-04-05 22:46:47', 'CREATED'),
(91, 55, '03:00:00', '2016-04-21', '5', 5, 'Asdas', 'phone', NULL, NULL, '2016-04-06 06:09:29', '2016-04-06 06:09:29', 'MODIFIED'),
(101, 2, '23:00:00', '2016-04-06', '5', 5, 'Asdas', 'phone', NULL, NULL, '2016-04-04 18:48:54', '2016-04-05 22:46:47', 'CREATED'),
(102, 18, '01:00:00', '2016-04-07', '5', 5, 'Asdas', 'phone', NULL, 'Lorem ipsum sdfdsfsdfdsfdsfsdf', '2016-04-06 06:18:50', '2016-04-06 06:18:50', 'MODIFIED'),
(104, 3, '09:01:00', '2016-04-06', '5', 5, 'As', 'phone', NULL, 'Lorem ipsum sdfdsfsdfdsfdsfsdf', '2016-04-06 04:51:23', '2016-04-06 04:51:23', ''),
(117, 55, '05:00:00', '2016-04-06', '10', 5, 'Andrjuha', 'phone', NULL, NULL, '2016-04-06 00:56:17', '2016-04-06 09:06:12', 'CANCELED'),
(137, 3, '20:50:00', '2016-04-20', '5', 20, 'Maxim Maximov', 'phone', '56880460', 'Birthday.', '2016-04-04 23:52:46', '2016-04-05 22:46:47', 'CREATED'),
(243, 2, '02:00:00', '2016-04-17', '2', 4, 'Leonid', 'phone', '3352492', 'sdfdsf', '2016-04-05 06:14:27', '2016-04-05 22:46:47', 'CREATED'),
(244, 18, '14:00:00', '2016-04-23', '4', 5, 'Denis', 'phone', NULL, NULL, '2016-04-06 00:53:10', '2016-04-06 00:53:10', 'CREATED'),
(246, 3, '18:00:00', '2016-04-26', '3', 2, 'Andrei', 'phone', NULL, NULL, '2016-04-05 06:22:52', '2016-04-05 22:46:47', 'CREATED'),
(253, 55, '20:00:00', '2016-04-27', '3', 3, 'asdasd', 'phone', NULL, NULL, '2016-04-06 00:56:27', '2016-04-06 00:56:27', 'CREATED'),
(254, 3, '20:00:00', '2016-04-27', '3', 3, 'asdasd', 'phone', NULL, NULL, '2016-04-05 06:33:58', '2016-04-05 22:46:47', 'CREATED'),
(255, 3, '03:00:00', '2016-04-13', '1', 1, 'asdas', 'phone', NULL, NULL, '2016-04-05 08:38:30', '2016-04-05 22:46:47', 'CREATED'),
(269, 17, '00:00:00', '2016-05-18', '5', 4, 'Artjom', 'phone', '56743354', 'difficulties', '2016-04-05 19:29:46', '2016-04-05 22:46:47', 'CREATED'),
(270, 2, '09:00:00', '2016-04-21', '10', 1, '213132132', 'email', NULL, NULL, '2016-04-05 19:33:07', '2016-04-05 22:46:47', 'CREATED'),
(272, 17, '17:00:00', '2016-04-22', '7', 4, 'Nikolai', 'other', NULL, NULL, '2016-04-05 22:49:11', '2016-04-05 22:50:43', 'CREATED'),
(273, 17, '12:00:00', '2016-04-27', '4', 3, 'Maksim', 'email', NULL, NULL, '2016-04-05 22:52:38', '2016-04-05 22:52:38', 'CREATED');

-- --------------------------------------------------------

--
-- Table structure for table `restaurant`
--

CREATE TABLE `restaurant` (
  `id` int(11) NOT NULL,
  `name` varchar(30) NOT NULL,
  `address` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `restaurant`
--

INSERT INTO `restaurant` (`id`, `name`, `address`) VALUES
(3, 'Tartusjan', 'Tartu'),
(18, 'P2rnuke', 'Pärnu'),
(55, 'Ristsalama', 'Maardu');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `reservation`
--
ALTER TABLE `reservation`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `restaurant`
--
ALTER TABLE `restaurant`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `reservation`
--
ALTER TABLE `reservation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=349;
--
-- AUTO_INCREMENT for table `restaurant`
--
ALTER TABLE `restaurant`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=91;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
